import java.util.*;

public class Homework12 {

    public static void main(String[] args) {

        Set<String> cohort1 = new HashSet<>();

        cohort1.add("United States");
        cohort1.add("United States");
        cohort1.add("Ukraine");
        cohort1.add("Mexico");

        Set<String> cohort2 = new HashSet<>();

        cohort2.add("United States");
        cohort2.add("Canada");
        cohort2.add("United States");
        cohort2.add("Mexico");

        // Separately
        // cohort 1
        System.out.println("cohort #1:");
        System.out.println(cohort1);

        System.out.println("\n");

        // cohort 2
        System.out.println("cohort #2");
        System.out.println(cohort2);

        System.out.println("\n");

        // cohorts in one line
        System.out.println("All cohorts");
        cohort2.addAll(cohort1);
        System.out.println(cohort2);

        // alphabetical order
        System.out.println("\n");
        System.out.println("Alphabetical Order");

        Set<String> cohort3 = new TreeSet<>(cohort1);

        cohort3.addAll(cohort2);

        System.out.println(cohort3);

        System.out.println("\n");

        // Common Countries
        System.out.println("Common Countries");
        Set<String> cohort4 = new HashSet<>(cohort1);

        cohort4.containsAll(cohort2);
        System.out.println(cohort4);

    }

}

